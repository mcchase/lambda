#http://ipvigilante.com/3.226.16.208

import requests

req = requests.request("GET", 'https://ipvigilante.com/3.226.16.208')
location = req.json()

longitude = location["data"]['longitude']
latitude = location["data"]['latitude']
city = location["data"]['city_name']

print(longitude, latitude, city)


#------------  IN LAMBDA -----------

import json
from botocore.vendored import requests

def lambda_handler(event, context):
    req = requests.request("GET", 'https://ipvigilante.com')
    location = req.json()
    longitude = location["data"]['longitude']
    latitude = location["data"]['latitude']
    city = location["data"]['city_name']
    geo = { "longitude": longitude, "latitude": latitude, "city": city }
    return {
        'statusCode': 200,
        'body': geo
    }

-----COMPARE----

import json
from botocore.vendored import requests

def lambda_handler(event, context):

    ip = event['queryStringParameters']['ip']

    r = requests.get(f'https://ipvigilante.com/{ip}').json()
    lat = r.get('data').get('latitude')
    lon = r['data']['longitude']
    city = r['data']['city_name']


    res = { "lat": lat, "lon": lon, "city": city }
    return {
        'statusCode': 200,
        'body': json.dumps(res)
    }
